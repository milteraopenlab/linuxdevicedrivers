#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/fs.h>

#define DEV_COUNT       2
#define MAX_DATA_LEN    30

static int mtchardev_open(struct inode *inode, struct file *file);
static int mtchardev_release(struct inode *inode, struct file *file);
static ssize_t mtchardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t mtchardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);

static const struct file_operations mtchardev_fops = {
    .owner      = THIS_MODULE,
    .open       = mtchardev_open,
    .release    = mtchardev_release,
    .read       = mtchardev_read,
    .write       = mtchardev_write
};

struct mtchar_device_data {
    struct cdev cdev;
};

static int dev_major = 0;
static struct class *mtchardev_class = NULL;
static struct mtchar_device_data mtchardev_data[DEV_COUNT];

static int mtchardev_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static int __init mtchardev_init(void)
{
    int err, i;
    dev_t dev;

    err = alloc_chrdev_region(&dev, 0, DEV_COUNT, "mtchardev");
    if(err < 0)
        return err;

    dev_major = MAJOR(dev);

    mtchardev_class = class_create(THIS_MODULE, "mtchardev");
    mtchardev_class->dev_uevent = mtchardev_uevent;

    for (i = 0; i < DEV_COUNT; i++) {
        cdev_init(&mtchardev_data[i].cdev, &mtchardev_fops);
        mtchardev_data[i].cdev.owner = THIS_MODULE;

        cdev_add(&mtchardev_data[i].cdev, MKDEV(dev_major, i), 1);

        device_create(mtchardev_class, NULL, MKDEV(dev_major, i), NULL, "mtchardev-%d", i);
    }

    return 0;
}

static void __exit mtchardev_exit(void)
{
    int i;

    for (i = 0; i < DEV_COUNT; i++) {
        device_destroy(mtchardev_class, MKDEV(dev_major, i));
    }

    class_unregister(mtchardev_class);
    class_destroy(mtchardev_class);

    unregister_chrdev_region(MKDEV(dev_major, 0), MINORMASK);
}

static int mtchardev_open(struct inode *inode, struct file *file)
{
    printk("MTCHARDEV: Device open\n");
    return 0;
}

static int mtchardev_release(struct inode *inode, struct file *file)
{
    printk("MTCHARDEV: Device close\n");
    return 0;
}

static ssize_t mtchardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    uint8_t *data = "Miltera Linux Lab\n";
    size_t datalen = strlen(data);

    printk("Reading device: %d\n", MINOR(file->f_path.dentry->d_inode->i_rdev));

    if (count > datalen) {
        count = datalen;
    }

    if (copy_to_user(buf, data, count)) {
        return -EFAULT;
    }

    return count;
}

static ssize_t mtchardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    size_t datalen = MAX_DATA_LEN, ncopied;
    uint8_t databuf[MAX_DATA_LEN];

    printk("Writing device: %d\n", MINOR(file->f_path.dentry->d_inode->i_rdev));

    if (count < MAX_DATA_LEN) {
        datalen = count;
    }

    ncopied = copy_from_user(databuf, buf, datalen);

    if (ncopied == 0) {
        printk("Copied %zd bytes from the user\n", datalen);
    } else {
        printk("Could't copy %zd bytes from the user\n", ncopied);
    }

    databuf[datalen] = 0;

    printk("Data from the user: %s\n", databuf);

    return count;
}

module_init(mtchardev_init);
module_exit(mtchardev_exit);

MODULE_DESCRIPTION("Character device module with class");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
