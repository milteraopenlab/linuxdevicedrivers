//===========================================================================
// C01: FILE DESCRIPTION
//===========================================================================
/** @file cdev_user.c
 *  @author Mehmet Kurnaz
 *	@version 0.1
 *	@date 05.02.2021
 *
 *  @brief Karakter surucu icin kullanıcı seviyesi ornek 
 *	uygulama.
 *
 */
//===========================================================================

//===========================================================================
// C02: INCLUDE FILES
//===========================================================================
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

//===========================================================================
// C03: PRIVATE CONSTANT DEFINITIONS
//===========================================================================
#define BUFFER_SIZE	64

//===========================================================================
// C04: PRIVATE DATA TYPES
//===========================================================================

//===========================================================================
// C05: PRIVATE FUNCTION PROTOTYPES
//===========================================================================

//===========================================================================
// C06: PRIVATE DATA DECLARATIONS
//===========================================================================

//===========================================================================
// C07: GLOBAL DATA DECLARATIONS
//===========================================================================

//===========================================================================
// C08: PRIVATE FUNCTIONS
//===========================================================================

//===========================================================================
// C09: GLOBAL FUNCTIONS
//===========================================================================


int main(int argc, char **argv)
{
    int fd;
    char buffer[BUFFER_SIZE];
    int read_len, write_len;

    if( argc < 2){
	printf("Usage: %s <dev_path>\n", argv[0]);
	return -1;
    }

    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
	printf("Coulnd't open %s\r\n", argv[1]);
	return -1;
    }

    printf("Reading from device(%s)\n", argv[1]);
    read_len = read(fd, buffer, BUFFER_SIZE);
    if(read_len < 0) {
	printf("Coulnd't read from device(%s)\n", argv[1]);
	return -1;
    }

    buffer[read_len] = '\0';
    printf("Device Data:%s\n", buffer);


    printf("Writing to device(%s)\n", argv[1]);
    strcpy(buffer, "Hello from user");
    write_len = write(fd, buffer, strlen(buffer));
    if(write_len < 0) {
	printf("Coulnd't write to device(%s)\n", argv[1]);
	return -1;
    }
    printf("Write Data(%d):%s\n", write_len, buffer);

    close(fd);

    return 0;
}


//===========================================================================
// C10: END OF CODE
//===========================================================================
