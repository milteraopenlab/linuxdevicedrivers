#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/fs.h>

#define MAX_DATA_LEN    30

static int mtchardev_open(struct inode *inode, struct file *file);
static int mtchardev_release(struct inode *inode, struct file *file);
static ssize_t mtchardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t mtchardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);

static const struct file_operations mtchardev_fops = {
    .owner      = THIS_MODULE,
    .open       = mtchardev_open,
    .release    = mtchardev_release,
    .read       = mtchardev_read,
    .write       = mtchardev_write
};

struct mychar_device_data {
    struct cdev cdev;
};

static int dev_major = 0;
static struct mychar_device_data mtchardev_data;

static int __init mtchardev_init(void)
{
    int err;
    dev_t dev;

    err = alloc_chrdev_region(&dev, 0, 1, "mtchardev");
    if(err < 0)
        return err;

    dev_major = MAJOR(dev);

    cdev_init(&mtchardev_data.cdev, &mtchardev_fops);
    mtchardev_data.cdev.owner = THIS_MODULE;

    cdev_add(&mtchardev_data.cdev, MKDEV(dev_major, 0), 1);

    printk("MTCharDev Major:%d\n", dev_major);

    return 0;
}

static void __exit mtchardev_exit(void)
{

    cdev_del(&mtchardev_data.cdev);
    unregister_chrdev_region(MKDEV(dev_major, 0), MINORMASK);
}

static int mtchardev_open(struct inode *inode, struct file *file)
{
    printk("MTCHARDEV: Device open\n");
    return 0;
}

static int mtchardev_release(struct inode *inode, struct file *file)
{
    printk("MTCHARDEV: Device close\n");
    return 0;
}

static ssize_t mtchardev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    uint8_t *data = "Miltera Linux Lab!\n";
    size_t datalen = strlen(data);

    printk("Reading device\n");

    if (count > datalen) {
        count = datalen;
    }

    if (copy_to_user(buf, data, count)) {
        return -EFAULT;
    }

    return count;
}

static ssize_t mtchardev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    size_t datalen = MAX_DATA_LEN, ncopied;
    uint8_t databuf[MAX_DATA_LEN];

    printk("Writing device\n");

    if (count < MAX_DATA_LEN) {
        datalen = count;
    }

    ncopied = copy_from_user(databuf, buf, datalen);

    if (ncopied == 0) {
        printk("Copied %zd bytes from the user\n", datalen);
    } else {
        printk("Could't copy %zd bytes from the user\n", ncopied);
    }

    databuf[datalen] = 0;

    printk("Data from the user: %s\n", databuf);

    return count;
}

module_init(mtchardev_init);
module_exit(mtchardev_exit);

MODULE_DESCRIPTION("Character device module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");