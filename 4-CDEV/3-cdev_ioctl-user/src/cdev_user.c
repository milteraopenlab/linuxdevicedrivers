//===========================================================================
// C01: FILE DESCRIPTION
//===========================================================================
/** @file cdev_user.c
 *  @author Mehmet Kurnaz
 *	@version 0.1
 *	@date 05.02.2021
 *
 *  @brief Karakter surucu icin kullanıcı seviyesi ornek 
 *	uygulama.
 *
 */
//===========================================================================

//===========================================================================
// C02: INCLUDE FILES
//===========================================================================
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

//===========================================================================
// C03: PRIVATE CONSTANT DEFINITIONS
//===========================================================================
#define MTIOCTL_WRITE_VALUE     _IOW(111, 0, int32_t*)
#define MTIOCTL_READ_VALUE      _IOR(111, 1, int32_t*)
#define MTIOCTL_WRITE_BUFFER    _IOW(111, 2, int32_t*)
#define MTIOCTL_READ_BUFFER     _IOR(111, 3, int32_t*)

//===========================================================================
// C04: PRIVATE DATA TYPES
//===========================================================================

//===========================================================================
// C05: PRIVATE FUNCTION PROTOTYPES
//===========================================================================

//===========================================================================
// C06: PRIVATE DATA DECLARATIONS
//===========================================================================

//===========================================================================
// C07: GLOBAL DATA DECLARATIONS
//===========================================================================

//===========================================================================
// C08: PRIVATE FUNCTIONS
//===========================================================================

//===========================================================================
// C09: GLOBAL FUNCTIONS
//===========================================================================


int main(int argc, char **argv)
{
    int fd;
    unsigned long value;
    int status;

    if( argc < 2){
	    printf("Usage: %s <dev_path>\n", argv[0]);
	    return -1;
    }

    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
	    printf("Coulnd't open %s\r\n", argv[1]);
	    return -1;
    }

    printf("IOCTL_READ_VALUE from device(%s)\n", argv[1]);
    status = ioctl(fd, MTIOCTL_READ_VALUE, &value);
    if(status < 0) {
	    printf("ICTL_READ_VALUE error\n");
	    return -1;
    }
    printf("Value:%ld\r\n", value);

    printf("IOCTL_WRITE_VALUE(%ld) to device(%s)\n", value, argv[1]);
    status = ioctl(fd, MTIOCTL_WRITE_VALUE, &value);
    if(status < 0) {
	    printf("ICTL_WRITE_VALUE error\n");
	    return -1;
    }

    printf("IOCTL_READ_BUFFER from device(%s)\n", argv[1]);
    status = ioctl(fd, MTIOCTL_READ_BUFFER, &value);
    if(status < 0) {
	    printf("ICTL_READ_BUFFER error\n");
	    return -1;
    }
    printf("Value:%ld\r\n", value);

    printf("IOCTL_WRITE_BUFFER(%ld) to device(%s)\n", value, argv[1]);
    status = ioctl(fd, MTIOCTL_WRITE_BUFFER, &value);
    if(status < 0) {
	    printf("ICTL_WRITE_BUFFER error\n");
	    return -1;
    }

    return 0;
}


//===========================================================================
// C10: END OF CODE
//===========================================================================
