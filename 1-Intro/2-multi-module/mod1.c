#include <linux/module.h>
#include <linux/kernel.h>
#include "mod2.h"

static int n1, n2;

static int multi_mod_init(void)
{
    n1 = 1; n2 = 2;
    pr_info("n1: %d, n2:%d\n", n1, n2);

    return 0;
}

static void multi_mod_exit(void)
{
    pr_info("sum is %d\n", add(n1, n2));
}

module_init(multi_mod_init);
module_exit(multi_mod_exit);

MODULE_DESCRIPTION("Multi file module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");