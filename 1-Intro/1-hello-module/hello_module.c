#include <linux/module.h>
#include <linux/kernel.h>

static int hello_init(void)
{
    printk("Hello Module!\n");
    return 0;
}

static void hello_exit(void)
{
    printk("Goodbye Module!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_DESCRIPTION("Hello Module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");