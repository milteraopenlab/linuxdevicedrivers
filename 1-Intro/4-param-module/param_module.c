#include <linux/module.h>
#include <linux/kernel.h>



static char *param = "Default";

module_param(param, charp, 0000);
MODULE_PARM_DESC(param, "String parameter");

static int __init param_init(void)
{
    pr_info("Module paramater is \"%s\"\n", param);
    return 0;
}

static void __exit param_exit(void)
{
    pr_info("Exit, stage left\n");
}

module_init(param_init);
module_exit(param_exit);

MODULE_DESCRIPTION("Command line parameter module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");