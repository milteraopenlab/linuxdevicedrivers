#include <linux/module.h>
#include <linux/kernel.h>

static int oops_module_init(void)
{
    char *p = 0;

    pr_info("before oops\n");
    *p = 0x61;
    pr_info("after oops\n");

    return 0;
}

static void oops_module_exit(void)
{
    pr_info("module goes all out\n");
}

module_init(oops_module_init);
module_exit(oops_module_exit);


MODULE_DESCRIPTION("oops module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");