#include <linux/init.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <linux/slab.h>

void my_work_handler_1(struct work_struct * work);
DECLARE_WORK(my_work_1, my_work_handler_1);

void my_work_handler_2(struct work_struct * work);
DECLARE_WORK(my_work_2, my_work_handler_2);

static DEFINE_MUTEX(a);
int global_variable;

void my_work_handler_1(struct work_struct *work)
{
    printk("WorkQueue handler-1\r\n");
    mutex_lock( &a );
    global_variable++;
    mutex_unlock( &a );
}

void my_work_handler_2(struct work_struct *work)
{
    printk("WorkQueue handler-2\r\n");
    mutex_lock( &a );
    global_variable++;
    mutex_unlock( &a );
}

static int wq_init(void)
{
    printk("WQ Init\r\n");
    global_variable = 0;
    schedule_work( &my_work_1 );
    schedule_work( &my_work_2 );

    return 0;
}

static void wq_exit(void)
{
    flush_scheduled_work();
}

module_init(wq_init);
module_exit(wq_exit);

MODULE_DESCRIPTION("WorkQueue module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");