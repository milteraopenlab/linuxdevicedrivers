#include <linux/module.h>
#include <linux/kthread.h>


void timer_func_1(struct timer_list *unused);
void timer_func_2(struct timer_list *unused);

static DEFINE_TIMER(timer_1, timer_func_1);
static DEFINE_TIMER(timer_2, timer_func_2);

static DEFINE_SPINLOCK(ktimer_lock);
int global_variable;


void timer_func_1(struct timer_list *unused)
{
    printk("%s\r\n", __func__);
    spin_lock(&ktimer_lock);
    global_variable++;
    spin_unlock(&ktimer_lock);
}

void timer_func_2(struct timer_list *unused)
{
    printk("%s\r\n", __func__);
    spin_lock(&ktimer_lock);
    global_variable++;
    spin_unlock(&ktimer_lock);
}


int init_module(void)
{
    unsigned long seconds = 3;
    global_variable = 0;

    mod_timer(&timer_1, jiffies + seconds * HZ);
    mod_timer(&timer_2, jiffies + seconds * HZ);

    return 0;
}

void exit_module(void)
{
    del_timer_sync(&timer_1);
    del_timer_sync(&timer_2);
}

MODULE_DESCRIPTION("KTimer module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
