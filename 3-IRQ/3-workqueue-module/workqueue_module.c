#include <linux/init.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <linux/slab.h>

struct workqueue_struct *wq;

struct work_data {
    struct work_struct work;
    int data;
};

static void work_handler(struct work_struct *work)
{
    struct work_data * data = (struct work_data *)work;
    printk("WorkQueue Handler\r\n");
    kfree(data);
}


static int wq_init(void)
{
    struct work_data * data;

    printk("WorkQueue Init\r\n");
    wq = create_workqueue("wq_test");
    data = kmalloc(sizeof(struct work_data), GFP_KERNEL);
    INIT_WORK(&data->work, work_handler);
    queue_work(wq, &data->work);

    return 0;
}

static void wq_exit(void)
{
    flush_workqueue(wq);
    destroy_workqueue(wq);
}

module_init(wq_init);
module_exit(wq_exit);

MODULE_DESCRIPTION("WorkQueue module with new queue");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");