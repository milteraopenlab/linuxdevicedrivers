#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>


void my_tasklet_function_1( unsigned long data );
void my_tasklet_function_2( unsigned long data );

char my_tasklet_data[]="my_tasklet_function was called";

static DEFINE_SPINLOCK(lock);
int global_variable;

DECLARE_TASKLET( my_tasklet_1, my_tasklet_function_1, 
         (unsigned long) &my_tasklet_data );

DECLARE_TASKLET( my_tasklet_2, my_tasklet_function_2, 
         (unsigned long) &my_tasklet_data );


void my_tasklet_function_1( unsigned long data )
{
    printk( "%s-1\n", (char *)data );

    spin_lock( &lock );
    global_variable++;
    spin_unlock( &lock );


    return;
}

void my_tasklet_function_2( unsigned long data )
{
    printk( "%s-2\n", (char *)data );

    spin_lock( &lock );
    global_variable++;
    spin_unlock( &lock );

    return;
}


int tasklet_module_init( void )
{
    global_variable = 0;

    tasklet_schedule( &my_tasklet_1 ); // TASKLET_SOFTIRQ
    /*tasklet_hi_schedule( &my_tasklet );*/ // HI_SOFTIRQ

    tasklet_schedule( &my_tasklet_2 ); // TASKLET_SOFTIRQ
    /*tasklet_hi_schedule( &my_tasklet );*/ // HI_SOFTIRQ

    return 0;
}

void tasklet_module_exit( void )
{
    tasklet_kill( &my_tasklet_1 );
    tasklet_kill( &my_tasklet_2 );

    return;
}

module_init(tasklet_module_init);
module_exit(tasklet_module_exit);

MODULE_DESCRIPTION("Tasklet module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");