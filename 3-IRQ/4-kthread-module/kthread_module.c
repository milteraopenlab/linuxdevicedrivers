#include <linux/module.h>
#include <linux/kthread.h>


static struct task_struct *thread_a;
static struct task_struct *thread_b;

DEFINE_MUTEX(kthread_mutex);
int global_variable;

int thread_func_a(void *unused)
{
    mutex_lock(&kthread_mutex);
    global_variable++;
    mutex_unlock(&kthread_mutex);

    return 0;
}

int thread_func_b(void *unused)
{
    mutex_lock(&kthread_mutex);
    global_variable++;
    mutex_unlock(&kthread_mutex);

    return 0;
}

int init_module(void)
{
    global_variable = 0;
    thread_a = kthread_run(thread_func_a, NULL, "thread_a");
    thread_b = kthread_run(thread_func_b, NULL, "thread_b");

    return 0;
}

void exit_module(void)
{
    kthread_stop(thread_a);
    kthread_stop(thread_b);
}


































MODULE_DESCRIPTION("KThread module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
