#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/genhd.h>
#include <linux/fs.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/vmalloc.h>


#define MTBLOCK_MAJOR		222
#define MTBLOCK_NAME		"mtblock"
#define MTBLOCK_MINORS		1
#define NR_SECTORS			128

#define KERNEL_SECTOR_SIZE	512

static struct mtblock_dev {
	spinlock_t lock;
	struct request_queue *queue;
	struct gendisk *gd;
	u8 *data;
	size_t size;
} g_dev;

static int mtblock_open(struct block_device *bdev, fmode_t mode);
static void mtblock_release(struct gendisk *gd, fmode_t mode);

static const struct block_device_operations my_block_ops = {
	.owner = THIS_MODULE,
	.open = mtblock_open,
	.release = mtblock_release
};

static int mtblock_open(struct block_device *bdev, fmode_t mode)
{
	return 0;
}

static void mtblock_release(struct gendisk *gd, fmode_t mode)
{
}

static void mtblock_transfer(struct mtblock_dev *dev, sector_t sector,
		unsigned long len, char *buffer, int dir)
{
	unsigned long offset = sector * KERNEL_SECTOR_SIZE;

	if ((offset + len) > dev->size)
		return;

	if (dir == 1)		/* write */
		memcpy(dev->data + offset, buffer, len);
	else
		memcpy(buffer, dev->data + offset, len);
}

static void mtblock_request(struct request_queue *q)
{
	struct request *rq;
	struct mtblock_dev *dev = q->queuedata;

	while (1) {

		rq = blk_fetch_request(q);
		if (rq == NULL)
			break;

		if (blk_rq_is_passthrough(rq)) {
			printk(KERN_NOTICE "Skip non-fs request\n");
			__blk_end_request_all(rq, -EIO);
			continue;
		}

		printk("request received: pos=%llu bytes=%u "
			"cur_bytes=%u dir=%c\n",
			(unsigned long long) blk_rq_pos(rq),
			blk_rq_bytes(rq), blk_rq_cur_bytes(rq),
			rq_data_dir(rq) ? 'W' : 'R');

		mtblock_transfer(dev, blk_rq_pos(rq),
				  blk_rq_bytes(rq),
				  bio_data(rq->bio), rq_data_dir(rq));

		__blk_end_request_all(rq, 0);
	}
}

static int create_block_device(struct mtblock_dev *dev)
{
	int err;

	dev->size = NR_SECTORS * KERNEL_SECTOR_SIZE;
	dev->data = kmalloc(dev->size, GFP_KERNEL);
	if (dev->data == NULL) {
		printk(KERN_ERR "vmalloc: out of memory\n");
		err = -ENOMEM;
		goto out_vmalloc;
	}

	spin_lock_init(&dev->lock);
	dev->queue = blk_init_queue(mtblock_request, &dev->lock);
	if (dev->queue == NULL) {
		printk(KERN_ERR "blk_init_queue: out of memory\n");
		err = -ENOMEM;
		goto out_blk_init;
	}
	blk_queue_logical_block_size(dev->queue, KERNEL_SECTOR_SIZE);
	dev->queue->queuedata = dev;

	/* initialize the gendisk structure */
	dev->gd = alloc_disk(MTBLOCK_MINORS);
	if (!dev->gd) {
		printk(KERN_ERR "alloc_disk: failure\n");
		err = -ENOMEM;
		goto out_alloc_disk;
	}

	dev->gd->major = MTBLOCK_MAJOR;
	dev->gd->first_minor = 0;
	dev->gd->fops = &my_block_ops;
	dev->gd->queue = dev->queue;
	dev->gd->private_data = dev;
	snprintf(dev->gd->disk_name, DISK_NAME_LEN, MTBLOCK_NAME);
	set_capacity(dev->gd, NR_SECTORS);

	add_disk(dev->gd);

	return 0;

out_alloc_disk:
	blk_cleanup_queue(dev->queue);
out_blk_init:
	vfree(dev->data);
out_vmalloc:
	return err;
}

static int __init mtblock_init(void)
{
	int err = 0;

	err = register_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
	if (err < 0) {
		printk(KERN_ERR "register_blkdev: unable to register\n");
		return err;
	}

	err = create_block_device(&g_dev);
	if (err < 0){
		unregister_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
		return err;
	}

	return 0;
}

static void delete_block_device(struct mtblock_dev *dev)
{
	if (dev->gd) {
		del_gendisk(dev->gd);
		put_disk(dev->gd);
	}
	if (dev->queue)
		blk_cleanup_queue(dev->queue);
	if (dev->data)
		vfree(dev->data);
}

static void __exit mtblock_exit(void)
{
	delete_block_device(&g_dev);

	unregister_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
}

module_init(mtblock_init);
module_exit(mtblock_exit);

MODULE_DESCRIPTION("Block device module for ramdisk, linuıx-4.x");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");