#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/blk_types.h>
#include <linux/blkdev.h>
#include <linux/blk-mq.h>
#include <uapi/linux/hdreg.h>
#include <uapi/linux/cdrom.h>


#define MTBLOCK_NAME  	"mtblock"
#define MTBLOCK_MAJOR	222
#define MTBLOCK_SIZE  	(128 * PAGE_SIZE) 

typedef struct {
    sector_t capacity;
    u8 *data;   /* Data buffer to emulate real storage device */
    atomic_t open_counter; /* atomic value to count open */
    struct blk_mq_tag_set tag_set;
    struct request_queue *queue;
    struct gendisk *gdisk;
} MTBlockDev;
static MTBlockDev* mtblock_dev = NULL;


static int open(struct block_device *bdev, fmode_t mode);
static void release(struct gendisk *disk, fmode_t mode);
static int ioctl(struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg);
#ifdef CONFIG_COMPAT
static int compat_ioctl(struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg);
#endif

static const struct block_device_operations bdev_fops = {
    .owner = THIS_MODULE,
    .open = open,
    .release = release,
    .ioctl = ioctl,
#ifdef CONFIG_COMPAT
    .compat_ioctl = compat_ioctl,
#endif
};

static int open(struct block_device *bdev, fmode_t mode)
{
    MTBlockDev* dev = bdev->bd_disk->private_data;
    if (dev == NULL) {
        printk(KERN_WARNING "blockdev: invalid disk private_data\n");
        return -ENXIO;
    }

    atomic_inc(&dev->open_counter);

    printk(KERN_WARNING "blockdev: device was opened\n");

    return 0;
}

static void release(struct gendisk *disk, fmode_t mode)
{
    MTBlockDev* dev = disk->private_data;
    if (dev) {
        atomic_dec(&dev->open_counter);

        printk(KERN_WARNING "blockdev: device was closed\n");
    }
    else
        printk(KERN_WARNING "blockdev: invalid disk private_data\n");
}

static int getgeo(MTBlockDev* dev, struct hd_geometry* geo)
{
    sector_t quotient;

    geo->start = 0;
    if (dev->capacity > 63) {

        geo->sectors = 63;
        quotient = (dev->capacity + (63 - 1)) / 63;

        if (quotient > 255) {
            geo->heads = 255;
            geo->cylinders = (unsigned short)((quotient + (255 - 1)) / 255);
        }
        else {
            geo->heads = (unsigned char)quotient;
            geo->cylinders = 1;
        }
    }
    else {
        geo->sectors = (unsigned char)dev->capacity;
        geo->cylinders = 1;
        geo->heads = 1;
    }
    return 0;
}

static int ioctl(struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg)
{
    int ret = -ENOTTY;
    MTBlockDev* dev = bdev->bd_disk->private_data;

    printk(KERN_WARNING "blockdev: ioctl %x received\n", cmd);

    switch (cmd) {
        case HDIO_GETGEO:
        {
            struct hd_geometry geo;

            ret = getgeo(dev, &geo );
            if (copy_to_user((void *)arg, &geo, sizeof(struct hd_geometry)))
                ret = -EFAULT;
            else
                ret = 0;
            break;
        }
        case CDROM_GET_CAPABILITY: //0x5331  / * get capabilities * / 
        {
            struct gendisk *disk = bdev->bd_disk;

            if (bdev->bd_disk && (disk->flags & GENHD_FL_CD))
                ret = 0;
            else
                ret = -EINVAL;
            break;
        }
    }

    return ret;
}

#ifdef CONFIG_COMPAT
static int compat_ioctl(struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg)
{
    // CONFIG_COMPAT is to allow running 32-bit userspace code on a 64-bit kernel
    return -ENOTTY; // not supported
}
#endif

static int blockdev_allocate_buffer(MTBlockDev* dev)
{
    dev->capacity = MTBLOCK_SIZE >> SECTOR_SHIFT;
    dev->data = kmalloc(dev->capacity << SECTOR_SHIFT, GFP_KERNEL); 
    if (dev->data == NULL) {
        printk(KERN_WARNING "blockdev: vmalloc failure.\n");
        return -ENOMEM;
    }

    return 0;
}

static void blockdev_free_buffer(MTBlockDev* dev)
{
    if (dev->data) {
        kfree(dev->data);

        dev->data = NULL;
        dev->capacity = 0;
    }
}

static void blockdev_remove_device(void)
{
    MTBlockDev* dev = mtblock_dev;
    if (dev == NULL)
        return;

    if (dev->gdisk)
        del_gendisk(dev->gdisk);

    if (dev->queue) {
        blk_cleanup_queue(dev->queue);
        dev->queue = NULL;
    }

    if (dev->tag_set.tags)
        blk_mq_free_tag_set(&dev->tag_set);

    if (dev->gdisk) {
        put_disk(dev->gdisk);
        dev->gdisk = NULL;
    }

    blockdev_free_buffer(dev);

    kfree(dev);
    mtblock_dev = NULL;

    printk(KERN_WARNING "blockdev: simple block device was removed\n");
}

static int do_simple_request(struct request *rq, unsigned int *nr_bytes)
{
    int ret = 0;
    struct bio_vec bvec;
    struct req_iterator iter;
    MTBlockDev *dev = rq->q->queuedata;
    loff_t pos = blk_rq_pos(rq) << SECTOR_SHIFT;
    loff_t dev_size = (loff_t)(dev->capacity << SECTOR_SHIFT);

    printk(KERN_WARNING "blockdev: request start from sector %lld \n", blk_rq_pos(rq));
    
    rq_for_each_segment(bvec, rq, iter)
    {
        unsigned long b_len = bvec.bv_len;

        void* b_buf = page_address(bvec.bv_page) + bvec.bv_offset;

        if ((pos + b_len) > dev_size)
            b_len = (unsigned long)(dev_size - pos);

        if (rq_data_dir(rq))//WRITE
            memcpy(dev->data + pos, b_buf, b_len);
        else//READ
            memcpy(b_buf, dev->data + pos, b_len);

        pos += b_len;
        *nr_bytes += b_len;
    }

    return ret;
}

static blk_status_t queue_rq(struct blk_mq_hw_ctx *hctx, const struct blk_mq_queue_data* bd)
{
    unsigned int nr_bytes = 0;
    blk_status_t status = BLK_STS_OK;
    struct request *rq = bd->rq;

    //we cannot use any locks that make the thread sleep
    blk_mq_start_request(rq);

    if (do_simple_request(rq, &nr_bytes) != 0)
        status = BLK_STS_IOERR;

    printk(KERN_WARNING "blockdev: request process %d bytes\n", nr_bytes);

    if (blk_update_request(rq, status, nr_bytes)) //GPL-only symbol
        BUG();
    __blk_mq_end_request(rq, status);

    return BLK_STS_OK;
}

static struct blk_mq_ops mq_ops = {
    .queue_rq = queue_rq,
};


static int blockdev_add_device(void)
{
    int ret = 0;
	struct request_queue *queue;
	struct gendisk *disk;

    MTBlockDev* dev = kzalloc(sizeof(MTBlockDev), GFP_KERNEL);
    if (dev == NULL) {
        printk(KERN_WARNING "blockdev: unable to allocate %ld bytes\n", sizeof(MTBlockDev));
        return -ENOMEM;
    }
    mtblock_dev = dev;

    do{
        ret = blockdev_allocate_buffer(dev);
        if(ret)
            break;

		//configure tag_set
		dev->tag_set.ops = &mq_ops;
		dev->tag_set.nr_hw_queues = 1;
		dev->tag_set.queue_depth = 128;
		dev->tag_set.numa_node = NUMA_NO_NODE;
		dev->tag_set.flags = BLK_MQ_F_SHOULD_MERGE ;
		dev->tag_set.driver_data = dev;

		ret = blk_mq_alloc_tag_set(&dev->tag_set);
		if (ret) {
			printk(KERN_WARNING "blockdev: unable to allocate tag set\n");
			break;
		}
	

		//configure queue
		queue = blk_mq_init_queue(&dev->tag_set);
		if (IS_ERR(queue)) {
			ret = PTR_ERR(queue);
			printk(KERN_WARNING "blockdev: Failed to allocate queue\n");
			break;
		}
		dev->queue = queue;
	    dev->queue->queuedata = dev;

		// configure disk
		disk = alloc_disk(1); //only one partition 
		if (disk == NULL) {
			printk(KERN_WARNING "blockdev: Failed to allocate disk\n");
			ret = -ENOMEM;
			break;
		}

		disk->flags |= GENHD_FL_NO_PART_SCAN; //only one partition 
		//disk->flags |= GENHD_FL_EXT_DEVT;
		disk->flags |= GENHD_FL_REMOVABLE;

		disk->major = MTBLOCK_MAJOR;
		disk->first_minor = 0;
		disk->fops = &bdev_fops;
		disk->private_data = dev;
		disk->queue = dev->queue;
		sprintf(disk->disk_name, "%s%d", MTBLOCK_NAME, 0);
		set_capacity(disk, dev->capacity);

		dev->gdisk = disk;
		add_disk(disk);
	

        printk(KERN_WARNING "blockdev: simple block device was created\n");
    }while(false);

    if (ret){
        blockdev_remove_device();
        printk(KERN_WARNING "blockdev: Failed add block device\n");
    }

    return ret;
}

static int __init blockdev_init(void)
{
    int ret = 0;

    ret = register_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
    if (ret < 0){
        printk(KERN_WARNING "blockdev: unable to get major number\n");
        return -EBUSY;
    }

    ret = blockdev_add_device();
    if (ret)
        unregister_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
        
    return ret;
}

static void __exit blockdev_exit(void)
{
    blockdev_remove_device();
    unregister_blkdev(MTBLOCK_MAJOR, MTBLOCK_NAME);
}

module_init(blockdev_init);
module_exit(blockdev_exit);

MODULE_DESCRIPTION("Block device module for ramdisk, linux-5.x");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");