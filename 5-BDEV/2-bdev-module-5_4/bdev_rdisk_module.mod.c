#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(.gnu.linkonce.this_module) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section(__versions) = {
	{ 0xc79d2779, "module_layout" },
	{ 0xb5a459dc, "unregister_blkdev" },
	{ 0xed0598f3, "device_add_disk" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x6186ad0b, "__alloc_disk_node" },
	{ 0xb99f8a40, "blk_mq_init_queue" },
	{ 0x14b33900, "blk_mq_alloc_tag_set" },
	{ 0xb8b9f817, "kmalloc_order_trace" },
	{ 0x26c2e0b5, "kmem_cache_alloc_trace" },
	{ 0x8537dfba, "kmalloc_caches" },
	{ 0x71a50dbc, "register_blkdev" },
	{ 0x56470118, "__warn_printk" },
	{ 0xbb05bd99, "__blk_mq_end_request" },
	{ 0x433fdf5c, "blk_update_request" },
	{ 0x69acdf38, "memcpy" },
	{ 0x7cd8d75e, "page_offset_base" },
	{ 0x97651e6c, "vmemmap_base" },
	{ 0x276d76d4, "blk_mq_start_request" },
	{ 0x37a0cba, "kfree" },
	{ 0xb377e32c, "put_disk" },
	{ 0xc8b65d06, "blk_mq_free_tag_set" },
	{ 0x7ff7df1b, "blk_cleanup_queue" },
	{ 0x3b7a3ee8, "del_gendisk" },
	{ 0xdecd0b29, "__stack_chk_fail" },
	{ 0xb44ad4b3, "_copy_to_user" },
	{ 0xc5850110, "printk" },
	{ 0xbdfb6dbb, "__fentry__" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "C35B5B4525460A3E13C5EAA");
