#include <linux/module.h>
#include <linux/kthread.h>

DEFINE_MUTEX(a);
DEFINE_MUTEX(b);

static struct task_struct *thread_a;
static struct task_struct *thread_b;

int thread_func_a(void *unused)
{
    mutex_lock(&a);
    pr_info("%s acquired A\n", __func__);
    mutex_lock(&b);
    pr_info("%s acquired B\n", __func__);

    mutex_unlock(&b);
    pr_info("%s released B\n", __func__);
    mutex_unlock(&a);
    pr_info("%s released A\n", __func__);

    return 0;
}

int thread_func_b(void *unused)
{
    mutex_lock(&b);
    pr_info("%s acquired B\n", __func__);
    mutex_lock(&a);
    pr_info("%s acquired A\n", __func__);

    mutex_unlock(&a);
    pr_info("%s released A\n", __func__);
    mutex_unlock(&b);
    pr_info("%s released B\n", __func__);

    return 0;
}

int init_module(void)
{
    thread_a = kthread_run(thread_func_a, NULL, "thread_a");
    thread_b = kthread_run(thread_func_b, NULL, "thread_b");

    return 0;
}

void exit_module(void)
{
    kthread_stop(thread_a);
    kthread_stop(thread_b);
}

MODULE_DESCRIPTION("Locking with mutex");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
