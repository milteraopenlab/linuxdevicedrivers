#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/sched/signal.h>



typedef struct {
    pid_t pid;
    unsigned long timestamp;
    struct list_head list;
}TaskInfo;

static struct list_head head;

static TaskInfo *task_info_alloc(int pid)
{
    TaskInfo *ti;

    ti = kmalloc(sizeof(*ti), GFP_KERNEL);
    if (ti == NULL)
	return NULL;
    ti->pid = pid;
    ti->timestamp = jiffies;

    return ti;
}

static void task_info_add_to_list(int pid)
{
    TaskInfo *ti;

    ti = task_info_alloc(pid);
    list_add(&ti->list, &head);
}

static void task_info_add_for_current(void)
{
    task_info_add_to_list(current->pid);
    task_info_add_to_list(current->parent->pid);
    task_info_add_to_list(next_task(current)->pid);
    task_info_add_to_list(next_task(next_task(current))->pid);
}

static void task_info_print_list(const char *msg)
{
    struct list_head *p;
    TaskInfo *ti;

    pr_info("%s: [ ", msg);
    list_for_each(p, &head) {
	ti = list_entry(p, TaskInfo, list);
	pr_info("(%d, %lu) ", ti->pid, ti->timestamp);
    }
    pr_info("]\n");
}

static void task_info_purge_list(void)
{
    struct list_head *p, *q;
    TaskInfo *ti;

    list_for_each_safe(p, q, &head) {
	ti = list_entry(p, TaskInfo, list);
	list_del(p);
	kfree(ti);
    }
}

static int list_init(void)
{
    INIT_LIST_HEAD(&head);

    task_info_add_for_current();

    return 0;
}

static void list_exit(void)
{
    task_info_print_list("before exiting");
    task_info_purge_list();
}

module_init(list_init);
module_exit(list_exit);

MODULE_DESCRIPTION("List module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
