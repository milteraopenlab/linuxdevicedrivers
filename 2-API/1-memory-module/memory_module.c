#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>

typedef struct {
    pid_t pid;
    unsigned long timestamp;
}TaskInfo;

static TaskInfo *task1, *task2, *task3, *task4;

static TaskInfo *task_info_alloc(int pid)
{
    TaskInfo *ti;

    ti = kmalloc(sizeof(*ti), GFP_KERNEL);
    if (ti == NULL)
	return NULL;
    ti->pid = pid;
    ti->timestamp = jiffies;

    return ti;
}

static int memory_init(void)
{
    task1 = task_info_alloc(current->pid);

    task2 = task_info_alloc(current->parent->pid);

    task3 = task_info_alloc(next_task(current)->pid);

    task4 = task_info_alloc(next_task(next_task(current))->pid);

    return 0;
}

static void memory_exit(void)
{
    printk("PID: %d, TS: %lu\n", task1->pid, task1->timestamp);
    printk("PID: %d, TS: %lu\n", task2->pid, task2->timestamp);
    printk("PID: %d, TS: %lu\n", task3->pid, task3->timestamp);
    printk("PID: %d, TS: %lu\n", task4->pid, task4->timestamp);

    /* Free allocated memeories */
    kfree(task1);
    kfree(task2);
    kfree(task3);
    kfree(task4);
}

module_init(memory_init);
module_exit(memory_exit);

MODULE_DESCRIPTION("Memory module");
MODULE_AUTHOR("Miltera");
MODULE_LICENSE("GPL");
